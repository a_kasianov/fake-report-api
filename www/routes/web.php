<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('doc');
});
Route::get('/reports', function () {
    return view('reports');
});
Route::get('/event', 'TaskController@event');
Route::get('/tasks', 'TaskController@allTasks');
Route::get('/report/{type}/{id}', 'TaskController@getReport');
Route::post('/task', 'TaskController@createTask');
