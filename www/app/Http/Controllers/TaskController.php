<?php

namespace App\Http\Controllers;

use App\Analytic;
use App\Events\TaskCreate;
use App\Http\Requests\TaskRequest;
use App\Jobs\RequestReport;
use App\Product;
use App\Services\ReportApiService;
use App\Statistic;
use App\Task;
use Illuminate\Support\Facades\Http;

class TaskController extends Controller
{

    public function allTasks()
    {
        $tasks = Task::orderBy('id', 'desc')->get();
        return response()->json($tasks);
    }

    public function createTask(TaskRequest $request, ReportApiService $reportApiService)
    {

        $report_id = $reportApiService->requestReport($request->type, $request->period, $request->kpi);
        if (!$report_id) {
            return response()->json([], 500);
        }
        try {
            $task = Task::create([
                'report_id' => $report_id,
                'type' => $request->type,
                'period' => $request->period,
                'progress' => 0,
                'kpi' => $request->kpi,
                'status' => 'processing'
            ]);
            broadcast(new TaskCreate($task));
            RequestReport::dispatch($task, $reportApiService);
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }
        return response()->json($task);
    }

    public function getReport($type, $id)
    {
        switch ($type) {
            case 'analytics':
                return response()->json(Analytic::where('report_id',$id)->limit(10)->get());
            case 'products':
                return response()->json(Product::where('report_id',$id)->limit(10)->get());
            case 'statistics':
                return response()->json(Statistic::where('report_id',$id)->limit(10)->get());
        }
        return response()->json([], 404);
    }
}
