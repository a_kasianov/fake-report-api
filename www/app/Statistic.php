<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{
    protected $fillable = ['report_id', 'first_name', 'last_name', 'address', 'phone', 'order_id', 'amount'];
    protected $hidden = ['id', 'report_id', 'updated_at', 'created_at'];
}
