<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['report_id', 'type', 'period', 'progress', 'kpi', 'status'];
    protected $hidden = ['updated_at', 'created_at'];
}
