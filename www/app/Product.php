<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['report_id', 'product', 'measurement', 'currency', 'price'];
    protected $hidden = ['id', 'report_id', 'updated_at', 'created_at'];
}
