<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analytic extends Model
{
    protected $fillable = ['report_id', 'company', 'url', 'address', 'phone', 'order_count', 'credit_card'];
    protected $hidden = ['id', 'report_id', 'updated_at', 'created_at'];
}
