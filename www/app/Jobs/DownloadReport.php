<?php

namespace App\Jobs;

use App\Analytic;
use App\Events\TasksUpdate;
use App\Product;
use App\Statistic;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;;
use League\Csv\Exception;
use League\Csv\Reader;

class DownloadReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 600;
    public $tries = 5;

    private $task;
    private $reportApiService;

    /**
     * Create a new job instance.
     *
     * @param $task
     * @param $reportApiService
     */
    public function __construct($task, $reportApiService)
    {
        $this->task = $task;
        $this->reportApiService = $reportApiService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $result = $this->reportApiService->downloadReport($this->task->report_id);
        if (!$result) {
            $this->task->status = 'error';
            $this->task->progress = 0;
            $this->task->save();
            broadcast(new TasksUpdate($this->task));
            return;
        }
        try {
            $reader = Reader::createFromString($result);
            $reader->setDelimiter(';');
            $reader->setHeaderOffset(0);
            $records = collect(iterator_to_array($reader, true))->chunk(500);
            foreach ($records as $chunk)
            {
                $items = $chunk->filter(function($item) {
                    return array_diff(array_keys($item), array_values($item));
                })->map(function ($item) {
                    $item['report_id'] = $this->task->report_id;
                    return $item;
                })->toArray();

                switch ($this->task->type) {
                    case 'analytics':
                        Analytic::insert($items);
                        break;
                    case 'products':
                        Product::insert($items);
                        break;
                    case 'statistics':
                        Statistic::insert($items);
                        break;
                }
            }
            $this->task->status = 'ready';
            $this->task->save();
            broadcast(new TasksUpdate($this->task));
        } catch (Exception $e) {
            $this->task->status = 'error';
            $this->task->save();
            broadcast(new TasksUpdate($this->task));
        }
    }
}
