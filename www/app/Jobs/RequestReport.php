<?php

namespace App\Jobs;

use App\Events\TasksUpdate;
use App\Services\ReportApiService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class RequestReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 120;
    public $tries = 5;

    private $task;
    private $reportApiService;

    /**
     * Create a new job instance.
     *
     * @param $task
     * @param ReportApiService $reportApiService
     */
    public function __construct($task, $reportApiService)
    {
        $this->task = $task;
        $this->reportApiService = $reportApiService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $result = $this->reportApiService->requestStatus($this->task->report_id);
        if (!$result) {
            $this->task->status = 'error';
            $this->task->progress = 0;
            $this->task->save();
            broadcast(new TasksUpdate($this->task));
            return;
        }
        if ($result['status'] == 'completed') {
            $this->task->status = 'downloading';
            $this->task->progress = 0;
            $this->task->save();
            broadcast(new TasksUpdate($this->task));
            DownloadReport::dispatch($this->task, $this->reportApiService);
        }
        if ($result['status'] == 'processing') {
            $this->task->progress = $result['percent'];
            $this->task->save();
            broadcast(new TasksUpdate($this->task));
            sleep(1);
            RequestReport::dispatch($this->task, $this->reportApiService);
        }
    }
}
