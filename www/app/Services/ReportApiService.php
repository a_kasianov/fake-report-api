<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class ReportApiService
{
    /**
     * Create a new job instance.
     *
     */
    public function __construct()
    {

    }

    public function requestReport($type, $period, $kpi)
    {
        $response = Http::post('http://nginx:81/reports/request', [
            "operator_id" => "operator001",
            "operator_key" => "secret",
            "type" => $type,
            "period" => $period,
            "kpi" => $kpi
        ]);
        if (!$response->successful()) {
            return false;
        }
        return $response->json()['report_id'] ?? false;
    }

    public function requestStatus($report_id)
    {
        $response = Http::post('http://nginx:81/reports/status', [
            "operator_id" => "operator001",
            "operator_key" => "secret",
            "report_id" => $report_id,
        ]);
        if (!$response->successful()) {
            return false;
        }
        return $response->json() ?? false;
    }

    public function downloadReport($report_id)
    {
        $response = Http::get(sprintf('http://nginx:81/reports/%d_report.csv', $report_id));
        if (!$response->successful()) {
            return false;
        }
        return $response->body() ?? false;
    }

}
