require('./bootstrap');
window.Vue = require('vue');

import { TablePlugin } from 'bootstrap-vue'
Vue.use(TablePlugin)

Vue.component('report', require('./components/Report.vue').default);
Vue.component('report-list', require('./components/ReportList.vue').default);
Vue.component('report-view', require('./components/ReportView.vue').default);

const app = new Vue({
    el: '#app',
});
