@extends('app')
@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <h3>Описание проекта</h3>
                <hr>
                <h5>Сервис Fake API для демо запросов</h5>
                <p>
                    Тестовый сервер создания отчетов. Написан на java. <a href="https://gitlab.com/a_kasianov/fake-report-api" target="_blank"><strong>Исходники проекта</strong></a>
                    <br>
                    Сервис генерит отчет от 30 сек до 2х минут, с набором рамдомных данных от 1К до 10К строк. Адрес сервиса <code class="bg-light">http://api.report.app-pf.host</code>
                </p>
                <strong>Api endpoints</strong>
                <ul>
                    <li><code class="bg-light">POST http://api.report.app-pf.host/reports/request</code> - создание отчета</li>
                    <li><code class="bg-light">POST http://api.report.app-pf.host/reports/status</code> - статус отчета</li>
                    <li><code class="bg-light">GET http://api.report.app-pf.host/reports/{id}_report.csv</code> - скачивание отчета</li>
                </ul>
                <strong>Формат запроса</strong>
                <br>
                <code class="bg-light">
                    { "operator_id": "operator001", "operator_key": "secret", "type": "products", "period": "2020-04-10", "kpi": 50 }
                </code>
                <br>
                <br>
                <h5>Сервис запросов отчетов</h5>
                <p>Адрес сервиса <code>http://report.app-pf.host</code></p>
                <p>Проект написан на Laravel, frontend VueJS. <a href="https://gitlab.com/a_kasianov/reports-service" target="_blank"><strong>Исходники проекта.</strong></a> Обработка очереди запросов к сервису генерации отчетов реализована через задачи. Задачи из очереди выпоняются последовательно. Очередь обрабатывается раз в секунду. Изменение прогресса оттправляется из задачи на frontend через WebSocket. Пользовательский интерфейс работает в режиме реального времени.</p>
                <p>Проект содержит полное окружение для локального запуска. Для первого запуска проекта необходимо выполнить команду <code class="bg-light">docker-compose up -d --build</code></p>
                <p>После успешного запуска всех контейнеров, необходимо запустить обработку очереди задач командой <code class="bg-light">docker-compose exec app php artisan queue:work &</code></p>
            </div>
        </div>
    </div>
@endsection
